---
title: "Setup Windows 10 for develop and daily usage"
date: 2018-12-01T18:10:14+01:00
tags: ['windows', 'wsl', 'ubuntu']
categories: ['tech']
author: "Joel"
noSummary: true
resizeImages: false
---

### Daily Apps

- firefox
- chrome
- 7zip
- vmware workstation
- office 2019/visio  activate with [ConsoleAct v2.x](https://www.solidfiles.com/folder/bd7165a0d4/)
- [adobe reader](https://get.adobe.com/cn/reader/enterprise/)
- calibre
- potplayer
- mailspring (windows mail is good enough, when you think not enough, use this, which sync more content and show context)

### Dev Environment

- Local program could install into `C:\Users\jm\AppData\Local\Programs` eg. hugo, ss
- jdk (not jre)
- dbeaver (sql manager)(do not install embeded jre)
- [hyper](https://hyper.is/) console simulator, other choices include [Cmder](http://cmder.net/), [ConsoleZ](https://github.com/cbucher/console), and Fish(oh-my-god-fish) as bash tool?
- typora
- vistual studio code(vscode)
    - theme: 
        ayu (dark bordered)
        min (dark)
    - extensions:
        gitlens
    - configures:
        user:     "python.venvPath": "e:/jm/dev/.envs"	# where `e:/jm/dev/.envs` hold a bunch of virtualenvs
    - [extension path](https://code.visualstudio.com/docs/editor/extension-gallery#_where-are-extensions-installed)
- ​Windows Subsystem Linux (WSL)
    1. enable windows feature WSL
    2. install ubuntu app
        type wsl in store search, and go to "run linux on windows" will show all (with no version select), do a further search with the linux distributions you want
        16.04	this is the production env for recstone site, so recommend to install
        18.04 	latest version of ubuntu, use for production
    3. To start ubuntu app from command run
        ```bash
        ubuntu1804	
        ubuntu1604
        ```
    4. setup WSL-Ubuntu for develop:
        1. setup tools and services
            ```bash
            # change source to aliyun
            sudo apt-get update
            sudo apt-get upgrade
            
            # gen locales
            locale -a	# show locales enabled
            sudo locale-gen "zh_CN.UTF-8"
            sudo locale-gen "zh_TW.UTF-8"
            sudo dpkg-reconfigure locales # leave with default .UTF-8, where you can change default locale
            
            # configure timezone
            sudo dpkg-reconfigure tzdata
            # check timezone
            timedatectl status | grep "Time zone"
            
            # set proxy
            export http_proxy=http://192.168.1.10:21080/
            export https_proxy=http://192.168.1.10:21080/
            
            # install open-vm-tools (if required, eg on vmware virtual machine)
            sudo apt-get install open-vm-tools open-vm-tools-desktop
            # install basic tools
            sudo apt-get install build-essential vim git curl wget -y
            # install packages required for python
            sudo apt-get install libmysqlclient-dev libreadline-dev libsqlite3-dev libbz2-dev libssl-dev libxml2-dev libxslt1-dev libffi-dev -y
            
            # install python with pyenv
            curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
            # install latest python
            pyenv install 3.7.1
            
            # install mysql(8.x) and postgresql(11.x)
            # mysql guide https://dev.mysql.com/downloads/repo/apt/
            # postgresql guide
            sudo apt-get install mysql-server postgresql
            
            # manage service of mysql and postgresql
            sudo service mysql status
            sudo service postgresql status
            ```
        2. configure mysql connection
            - check [this](https://dev.mysql.com/doc/refman/8.0/en/default-privileges.html) for post install actions 
            - use `mysql` to manager the database in command line, unlike in `postgresql` this tool could manage almost everything
                ```bash
                mysql -u root -p
                ```
            - `mysql` is configured to listen remote connection, just need to add user, check [this](https://dev.mysql.com/doc/refman/8.0/en/adding-users.html) to add users 

                ```
                mysql> CREATE USER 'jm'@'localhost' IDENTIFIED BY 'password';
                mysql> GRANT ALL PRIVILEGES ON *.* TO 'finley'@'localhost' WITH GRANT OPTION;
                mysql> CREATE USER 'jm'@'%' IDENTIFIED BY 'password';
                mysql> GRANT ALL PRIVILEGES ON *.* TO 'finley'@'%' WITH GRANT OPTION;
                ```
            - to change listen port add address edit file `/etc/mysql/mysql.conf.d/mysqld.cnf` set
                ```
                port    = 3306      # default to 3306
                listen  = 0.0.0.0   # default to 0.0.0.0
                ```
            - client requires specify properties:
                ```
                allowPublicKeyRetrieval =   true
                useSSL                  =   false
                ```
            - dump(backup) and restore a database
                ```bash
                # dump a database
                mysqldump -u jm -p db_name > dump.sql
                # restore a dump file
                mysql -u jm -p db_name < dump.sql
                ```
- xmind zen
- git
- sigil (epub builder)
- python (latest) for current user only
    ```bash
    pip install virtualenv
    virtualenv e:\jm\dev\.env\recstone
    e:\jm\dev\.env\recstone\Scripts\activate
    ```

