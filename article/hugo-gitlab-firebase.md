---
title: "Setup blog with hugo, gitlab and firebase"
date: 2018-12-03T18:10:14+01:00
tags: ['hugo', 'gitlab', 'firebase', 'ci']
categories: ['tech']
author: "Joel"
noSummary: true
resizeImages: false
---

### steps below
1. create a site with hugo new site path

2. add theme bilberry with submodule, and follow instructions of the theme

   ```bash
        git submodule add https://github.com/Lednerb/bilberry-hugo-theme.git themes/bilberry-hugo-theme
   ```

3. tweak hugo then try with `hugo server` to test the result
    - languages
    - title and such
    - width
    - algolia

4. create a project on `firebase` nothing special
5. [optional] setup firebase tools locally with node 
6. [optional] setup firebase config file. this could be done using `firebase init` or just drop in the file
    ```json
        // firebase.json
        {
            "hosting": {
                "public": "public",
                "ignore": [
                    "firebase.json",
                    "**/.*",
                    "**/node_modules/**"
                ]
            }
        }
    ```
7. [optional] run deploy locally by `firebase deploy`
8. generate firebase token by running `firebase login:ci` copy down the token value
9. config gitlab ci by create `.gitlab-ci.yml`
    ```yml
        variables:
            GIT_SUBMODULE_STRATEGY: recursive
            GIT_SSL_NO_VERIFY: "true"

        stages:
            - deploy

        deploy:
            stage: deploy
            image: nohitme/hugo-firebase
            script:
                # build site
                - cd ${CI_PROJECT_DIR}
                - hugo
                # upload
                - firebase deploy --project ${PROJECT_ID} --token ${FIREBASE_TOKEN}
            only:
                - master
    ```
10. config gitlab ci variables on the web page `Settings` -> `CI/DI` -> `Variables`. Notice there are 3 variables in the `.gitlab-ci.yml` the system will feed `CI_PROJECT_DIR`. `PROJECT_ID` and `FIREBASE_TOKEN` should be provided accordingly

11. commit to git lab it will start the build and deploy

12. seperate note content from note site.
    1. create a notes project 
    2. git submodule add notes-project content
    3. setup ci for notes project which call a trigger for the note-site project eg.
        ```yml
            # .gitlab-ci.yml
            # SITE_TOKEN see next step
            # REF_NAME branch name like master
            trigger_build:
            stage: deploy
            script:
                - "curl -X POST -F token=${SITE_TOKEN} -F ref=${REF_NAME} https://gitlab.com/api/v4/projects/9739876/trigger/pipeline"
        ```
    4. setup trigger for note-site projet `Settings` -> `CI\DI` ->`Pipeline triggers` create a new one and use the token for step 3
    After doing this, when notes repository changed, note-site will also trigger a build
12. [TODO] checkout the image `nohitme/hugo-firebase` from [docker hub](hub.docker.com)

13. [TODO] gitlab CI basic howto 
